<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ClientCreateCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName('oauth:client:create')
                ->setDescription('Creates a new client')
                ->addArgument('redirect-uri', InputArgument::REQUIRED, 'Sets the redirect uri. Use multiple times to set multiple uris.')
                ->addArgument('grant-type', InputArgument::REQUIRED, 'Set allowed grant type. Use multiple times to set multiple grant types.')
                ->addArgument('name', InputArgument::REQUIRED, 'Set client name.')
        ;
    }

    protected function interact(InputInterface $input, OutputInterface $output) {
        $defaultType1 = 'http://localhost/';
        $question1 = array(
            "<question>Enter a Redirect URI:</question> [<comment>$defaultType1</comment>] ",
        );

        $type1 = $this->getHelper('dialog')->askAndValidate($output, $question1, function($inputUri) {
            $url = filter_var($inputUri, FILTER_SANITIZE_URL);

            if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            } else {
                throw new \InvalidArgumentException('Enter a valid Redirect URI');
            }

            return $inputUri;
        }, 10, $defaultType1);

        $input->setArgument('redirect-uri', $type1);
        
        $defaultType2 = 1;
        $types = array(
            '1' => 'authorization_code',
            '2' => 'token',
            '3' => 'password',
            '4' => 'client_credentials',
            '5' => 'refresh_token',
            '6' => 'extensions',
        );
        $question2 = array(
            "<comment>1</comment>: Authorization code\n",
            "<comment>2</comment>: Token\n",
            "<comment>3</comment>: Password\n",
            "<comment>4</comment>: Client credentials\n",
            "<comment>5</comment>: Refresh token\n",
            "<comment>6</comment>: Extensions\n",
            "<question>Choose a Grant type:</question> [<comment>$defaultType2</comment>] ",
        );

        $type2 = $this->getHelper('dialog')->askAndValidate($output, $question2, function($typeInput) {
            if (!in_array($typeInput, array(1, 2, 3, 4, 5, 6))) {
                throw new \InvalidArgumentException('Invalid type');
            }

            return $typeInput;
        }, 10, $defaultType2);

        $defaultType3 = 'Client';
        $input->setArgument('grant-type', $types[$type2]);
        
        $question3 = array(
            "<question>Enter a name:</question> [<comment>$defaultType3</comment>] ",
        );

        $type3 = $this->getHelper('dialog')->askAndValidate($output, $question3, function($typeInput) {
            if ($typeInput === '') {
                throw new \InvalidArgumentException('Enter a valid name');
            }

            return $typeInput;
        }, 10, $defaultType3);

        $input->setArgument('name', $type3);
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $clientManager = $this->getContainer()->get('fos_oauth_server.client_manager.default');

        $client = $clientManager->createClient();
        $output->writeln(sprintf('Redirect URI: <info>%s</info>', $input->getArgument('redirect-uri')));
        $client->setRedirectUris(array($input->getArgument('redirect-uri')));
        $output->writeln(sprintf('Grant type: <info>%s</info>', $input->getArgument('grant-type')));
        $client->setAllowedGrantTypes(array($input->getArgument('grant-type')));
        $output->writeln(sprintf('Client name: <info>%s</info>', $input->getArgument('name')));
        $client->setName($input->getArgument('name'));

        $clientManager->updateClient($client);

        $output->writeln(sprintf('Added a new client with  public id <info>%s</info>.', $client->getPublicId()));
    }

}
