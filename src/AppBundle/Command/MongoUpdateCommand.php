<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MongoUpdateCommand extends ContainerAwareCommand
{
    protected $route;
    
    public function __construct()
    {
        parent::__construct();
        $this->route = 'cd ' . __DIR__ . '/../../../;php app/console ';
    }
    
    protected function configure()
    {
        $this->setName('mongo:update')
             ->setDescription('Delete and reconstruct database.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('Dropping Mongo document >> ');
        $output->writeln(shell_exec($this->route . 'doctrine:mongodb:schema:drop'));
        
        $output->write('Creating Mongo schema >> ');
        $output->writeln(shell_exec($this->route . 'doctrine:mongodb:schema:create'));

        $output->write('Loading Mongo fixtures >> ');
        $output->writeln(shell_exec($this->route . 'doctrine:mongodb:fixtures:load'));
        
        $output->writeln('Finished successfully.');
    }
}