var socket = io('http://127.0.0.1:1337/');
var name = '';
socket.on('connect', function () {
    socket.on('message', function (msg) {
        console.log(msg);
    });
    socket.on('write', function(char){
        $('#output-words').val(char);
    });
    socket.on('first', function(first){
        $('#challenger').text(first);
    });
    socket.on('challenger', function(challenger){
        $('#challenger').text(challenger);
    });
});
$('#join').on('click', function(){
    if ($('#name').val() !== '') {
        name = $('#name').val();
        $(this).parent().html('').text(name);
        socket.emit('join', { name: name });
    }
});